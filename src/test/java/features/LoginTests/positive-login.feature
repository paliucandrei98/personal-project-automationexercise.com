@UI @Positive @Login

Feature: Login tests - positive scenarios

    Scenario: Login with valid credentials

      Given Be on the Login Page
      When Enter email address "test.email@gmail.com"
      And Enter password "Password123"
      And Click on Login Button
      Then Logout button is displayed

@Cookie
    Scenario: Login on the test account using cookie
      Given Be on the Home page
      When Add 'session id cookie' and refresh
      Then Logout button is displayed
      And Delete 'session id cookie' cookie



