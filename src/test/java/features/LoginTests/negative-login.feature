@UI @Negative @Login

Feature: Login tests - negative scenarios

  Scenario: Login with correct email address and without password
    Given Be on the Login Page
    When  Enter email address "test.email@gmail.com"
    When  Click on Login Button
    Then Remain on the login page

  Scenario: Login without email address
    Given Be on the Login Page
    When  Enter password "Password123"
    When  Click on Login Button
    Then Remain on the login page

  Scenario: Log in with incorrect email and password
    Given Be on the Login Page
    When Enter email address "test.email2@gmail.com"
    And Enter password "Password1234"
    And Click on Login Button
    Then 'Email or password is incorrect' message is displayed

  Scenario Outline: Login by entering incorrect email addresses and correct password
    Given Be on the Login Page
    When Enter email address "<email address>"
    And Enter password "Password123"
    And Click on Login Button
    Then 'Email or password is incorrect' message is displayed

    Examples:
      | email address                |
      | test2.email@gmail.com        |
      | test.email@yahoo.com         |
      | not.a.real.email@address.net |

  Scenario Outline: Login by entering invalid email addresses and correct password
    Given Be on the Login Page
    When Enter email address "<email address>"
    And Enter password "Password123"
    And Click on Login Button
    Then Remain on the login page

    Examples:
      | email address                |
      | @.com                        |
      | gmail.com                    |
      | not.a.real.email.address.net |
      | Andrei Paliuc                |
      | username                     |
      | 123                          |
      | test2.email@%!@#             |


