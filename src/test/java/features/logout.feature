@UI @LogOut
  Feature: Logout tests

    Scenario Outline: Logout User

      Given User is logged with valid credentials
        | <email>    |
        | <password> |
      When  Logout button is displayed
      And Click on Logout Button
      Then Be on the Login or SignUp Page

      Examples:
        | email                | password    |
        | test.email@gmail.com | Password123 |