@UI @Positive @Product

  Feature: Products

    Scenario: Search product
      Given Be on the Home page
      When Go to the Products page
      Then All Products section is displayed on the page
      When Enter product in the search bar "t-shirt"
      And Click on the search button
      Then  Searched Products section is displayed on the page

    Scenario: Add product to cart and continue shopping
      Given Be on Products page
      When Hover over first product
      Then Add to cart button is displayed
      When Click on add to cart
      Then Confirmation Product has been added to cart is displayed
      When Click on the Continue Shopping Button
      Then User remains on the Products page

    Scenario: Add product to cart and view cart
      Given Be on Products page
      When Hover over first product
      Then Add to cart button is displayed
      When Click on add to cart
      Then Confirmation Product has been added to cart is displayed
      When Click on the View Cart Button
      Then User is moved to the view cart page
      And Cart is not empty

    Scenario: Place an order for a product
      Given Be logged in and on the Home page
      When Add product in the cart and view cart
      Then  Proceed to checkout is displayed
      When Click on Proceed to checkout button
      Then Checkout Information are displayed
      And Place order button is displayed
      When Click on place order button
      Then User is moved to Payment page
      When User enter Payment details "Andrei","1234-5678-9101","112","12","2045"
      And Click on Pay and Confirm Order page
      Then  Congratulations! Your order has been confirmed! message is displayed






      
