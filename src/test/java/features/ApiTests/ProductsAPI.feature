@API @Product
  Feature: API Products Tests

    Scenario: GET all products list
      Given Api url is "https://automationexercise.com/api/productsList"
      When Execute GET request on the url
      Then Verify Status code is 200


    Scenario: GET all product's brands list
      Given Api url is "https://automationexercise.com/api/brandsList"
      When Execute GET request on the url
      Then Verify Status code is 200


    Scenario: POST To All Products List
      Given Api url is "https://automationexercise.com/api/productsList"
      When Execute POST request on the url
      Then Verify Status code is 405
      #primesc cod 200 si ce apare jos e cod 405
  