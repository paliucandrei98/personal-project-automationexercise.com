@UI @Positive @Register-positive
  Feature: Register tests - positive scenarios

    Scenario: Register user with mandatory fields
      Given Be on the Login or SignUp Page
      When New User Signup! text is displayed
      And Enter name "Andrei"
      And Enter new email address "andrei1@email.com"
      And Click on the Signup button
      Then Signup form is displayed
      And Enter new password "Password123"
      And Enter First Name "Andrei"
      And Enter Last Name "TotAndrei"
      And Enter address "Street Name, nr1"
      And Enter State "Romania"
      And Enter City "Bucharest"
      And Enter Zipcode "007"
      And Enter mobile number "0123456789"
      And Click on the Create Account Button
      Then  Account created Text is displayed
      And Click on the continue button
      Then Be on the Home page




