@UI @Negative @Register
Feature: Register tests - negative scenarios

  Scenario: Register user without any data
    Given Be on the Login or SignUp Page
    When New User Signup! text is displayed
    And Enter name "Andrei"
    And Enter new email address "andrei1@email.com"
    And Click on the Signup button
    Then Signup form is displayed
    And Click on the Create Account Button
    Then Signup form is displayed


  Scenario: User is not taken to Signup Form without entering a name and password
    Given Be on the Login or SignUp Page
    And Click on the Signup button
    Then  Remain on the login page


  Scenario: Register user with an already used email address
    Given Be on the Login or SignUp Page
    And Enter name "Andrew"
    And Enter new email address "andrew@email.com"
    And Click on the Signup button
    Then Email Address already exist! error message is displayed