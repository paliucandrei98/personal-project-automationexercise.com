package stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import org.junit.Assert;
import org.openqa.selenium.Cookie;
import selenium_pom.HomePage;
import selenium_pom.LoginAndSignUpPage;

import static stepdefinitions.Hooks.driver;

public class LogoutSteps {
    LoginAndSignUpPage loginPage;
    HomePage homePage = new HomePage(driver);

    @Given("User is logged on the application")
    public void userIsLoggedOnTheApplication(){
        System.out.println("Logged on application using cookie");
        driver.get(HomePage.url);
        driver.manage().addCookie(new Cookie("sessionid","31ixjfotdo5hjsaonrjv8xojtsmr7uxf"));
        driver.navigate().refresh();
        Assert.assertTrue(homePage.logoutButtonIsDisplayed());
    }

    @And("Click on Logout Button")
    public void clickOnLogoutButton() {
        System.out.println("click on Logout button");
        homePage.clickLogoutButton();
    }
}
