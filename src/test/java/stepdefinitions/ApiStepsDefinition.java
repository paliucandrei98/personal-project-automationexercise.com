package stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.Assert;

public class ApiStepsDefinition {
    private Response response;

    @Given("Api url is {string}")
    public void ApiUrlIs(String url) {
        RestAssured.baseURI = url;
    }

    @When("Execute GET request on the url")
    public void executeGETRequestOnTheUrl() {
        response = RestAssured.get();
    }

    @Then("Verify Status code is {int}")
    public void verifyStatusCode(int expectedStatusCode) {
        Assert.assertEquals(expectedStatusCode, response.getStatusCode());
    }

    @When("Execute POST request on the url")
    public void executePOSTRequestOnTheUrl() {
        //modelul cu booking
        response = RestAssured.post();
//        response = RestAssured.given()
//                .contentType(ContentType.JSON)
//                .request()
//                .post();
    }
}
