package stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import selenium_pom.AccountCreatedPage;
import selenium_pom.LoginAndSignUpPage;
import selenium_pom.SignupPage;

import static stepdefinitions.Hooks.driver;

public class RegisterSteps {
    LoginAndSignUpPage loginAndSignUpPage;
    SignupPage signupPage = new SignupPage(driver);
    AccountCreatedPage accountCreatedPage = new AccountCreatedPage(driver);

    @Given("Be on the Login or SignUp Page")
    public void beOnTheLoginRegisterPage() {
        System.out.println("Login page opened");
        driver.get(LoginAndSignUpPage.url);
        loginAndSignUpPage = new LoginAndSignUpPage(driver);
    }

    @When("New User Signup! text is displayed")
    public void newUserSignupIsDisplayed() {
        System.out.println("Check New User Signup! text is displayed");
        Assert.assertTrue("Check New User Signup! is not displayed", loginAndSignUpPage.newUserSignUpTextIsDisplayed());
    }

    @And("Enter name {string}")
    public void enterName(String name) {
        System.out.println("Name inserted");
        loginAndSignUpPage.enterName(name);
    }

    @And("Enter new email address {string}")
    public void enterNewEmailAddress(String eMail) {
        System.out.println("New email address inserted");
        loginAndSignUpPage.enterRegisterEmailAddress(eMail);
    }

    @And("Click on the Signup button")
    public void clickOnTheSignupButton() {
        System.out.println("Click on Signup Button");
        loginAndSignUpPage.clickSignUp();
    }

    @And("Enter new password {string}")
    public void enterNewPassword(String password) {
        System.out.println("Password Inserted");
        signupPage.enterPassword(password);
    }

    @And("Enter First Name {string}")
    public void enterFirstName(String firstName) {
        System.out.println("First Name inserted");
        signupPage.enterFirstName(firstName);
    }

    @And("Enter Last Name {string}")
    public void enterLastName(String lastName) {
        System.out.println("Last name inserted");
        signupPage.enterLastName(lastName);
    }

    @And("Enter address {string}")
    public void enterAddress(String address) {
        System.out.println("Address inserted");
        signupPage.enterAddress(address);
    }

    @And("Enter State {string}")
    public void enterState(String state) {
        System.out.println("State inserted");
        signupPage.enterState(state);
    }

    @And("Enter City {string}")
    public void enterCity(String city) {
        System.out.println("City inserted");
        signupPage.enterCity(city);
    }

    @And("Enter Zipcode {string}")
    public void enterZipcode(String zipCode) {
        System.out.println("Zip code inserted");
        signupPage.enterZipCode(zipCode);
    }

    @And("Enter mobile number {string}")
    public void enterMobileNumber(String mobileNumber) {
        System.out.println("Mobile Number inserted");
        signupPage.enterMobileNumber(mobileNumber);
    }

    @And("Click on the Create Account Button")
    public void clickOnTheCreateAccountButton() {
        System.out.println("Click on create account");
        signupPage.clickCreateAccount();
    }

    @Then("Account created Text is displayed")
    public void userIsMovedToTheAccountCreatedPage() {
        System.out.println("Account created Text is displayed");
        signupPage.accountCreatedIsDisplayed();
    }

    @When("Click on the continue button")
    public void clickOnContinueButton() {
        System.out.println("Click on continue button");
        signupPage.clickContinue();
    }

    @And("Signup form is displayed")
    public void signupFormIsDisplayed() {
        System.out.println("Signup Form is displayed");
        Assert.assertTrue("Signup Form is not displayed", signupPage.signUpFormIsDisplayed());
    }

    @Then("Email Address already exist! error message is displayed")
    public void emailAddressAlreadyExistErrorMessageIsDisplayed() {
        System.out.println("Email Address already exist! error message is displayed");
        Assert.assertTrue("Email Address already exist! error message is not displayed", loginAndSignUpPage.emailAddressAlreadyExistsText());
    }
}
