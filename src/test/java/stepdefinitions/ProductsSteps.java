package stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import selenium_pom.LoginAndSignUpPage;
import selenium_pom.PaymentPage;
import selenium_pom.ProductsPage;
import java.time.Duration;
import static stepdefinitions.Hooks.driver;

public class ProductsSteps {
    ProductsPage productsPage = new ProductsPage(driver);
    LoginAndSignUpPage loginPage;
    PaymentPage paymentPage = new PaymentPage(driver);

    @When("Go to the Products page")
    public void goToTheProductsPage() {
        System.out.println("User is moved to the Products page");
        driver.navigate().to(ProductsPage.url);
    }

    @Then("All Products section is displayed on the page")
    public void allProductsSectionIsDisplayedOnThePage() {
        productsPage = new ProductsPage(driver);
        Assert.assertTrue(productsPage.setAllProductsSectionIsDisplayed());
        Assert.assertEquals("ALL PRODUCTS",driver.findElement(By.xpath("//*[@class=\"title text-center\"]")).getText());
    }

    @When("Enter product in the search bar {string}")
    public void enterProductInTheSearchBar(String product) {
        System.out.println("Product input entered");
        productsPage.enterProductName(product);
    }

    @And("Click on the search button")
    public void clickOnTheSearchButton() {
        System.out.println("Click on Search Button");
        productsPage.clickOnSearchButton();
    }

    @Then("Searched Products section is displayed on the page")
    public void searchedProductsSectionIsDisplayedOnThePage() {
        System.out.println("SEARCHED PRODUCTS is displayed on the page");
        Assert.assertTrue(productsPage.searchedProductsSectionIsDisplayed());
        Assert.assertEquals("SEARCHED PRODUCTS",driver.findElement(By.xpath("//*[@class=\"title text-center\"]")).getText());
    }

    @Given("Be on Products page")
    public void beOnProductsPage() {
        System.out.println("User is no the Products page");
        driver.get(ProductsPage.url);
        productsPage = new ProductsPage(driver);
    }

    @When("Hover over first product")
    public void hoverOverFirstProduct() {
        System.out.println("Mouse is over the first product image");
        Actions mouseHover = new Actions(driver);
        WebElement firstItem = driver.findElement(By.xpath("//*[@src=\"/get_product_picture/1\"]"));
        mouseHover.moveToElement(firstItem);
    }

    @Then("Add to cart button is displayed")
    public void addToCartButtonIsDisplayed() {
        System.out.println("Add to cart button is displayed");
        Assert.assertTrue(productsPage.addToCartButtonIsDisplayed());
    }

    @When("Click on add to cart")
    public void clickOnAddToCart() {
        System.out.println("Click on add to cart");
        productsPage.clickOnAddToCartButton();
    }

    @Then("Confirmation Product has been added to cart is displayed")
    public void confirmationProductHasBeenAddedToCartIsDisplayed() {
        WebDriverWait explicitWaitForConfirmationPopUp = new WebDriverWait(driver, Duration.ofSeconds(10));
        WebElement confirmationPopUp = driver.findElement((By.xpath("//*[@id=\"cartModal\"]/div/div/div[2]/p[1]")));
        explicitWaitForConfirmationPopUp.until(ExpectedConditions.visibilityOf(confirmationPopUp));
        Assert.assertTrue(productsPage.confirmationAddProductToCartIsDisplayed());
        System.out.println("Your product has been added to cart message is displayed");
    }

    @When("Click on the Continue Shopping Button")
    public void clickOnTheContinueShoppingButton() {
        System.out.println("Click on Continue Shopping button");
        productsPage.clickOnContinueShoppingButton();
    }

    @Then("User remains on the Products page")
    public void userRemainsOnTheProductsPage() {
        System.out.println("User remains on Products page");
        Assert.assertEquals("https://automationexercise.com/products",driver.getCurrentUrl());
    }

    @When("Click on the View Cart Button")
    public void clickOnTheViewCartButton() {
        System.out.println("Click on View Cart button");
        productsPage.clickOnViewCartFromConfirmationButton();
    }

    @Then("User is moved to the view cart page")
    public void userIsMovedToTheViewCartPage() {
        System.out.println("User is moved to the view cart page");
        Assert.assertEquals("https://automationexercise.com/view_cart",driver.getCurrentUrl());
    }

    @And("Cart is not empty")
    public void cartIsNotEmpty() {
        System.out.println("Cart is not empty");
        Assert.assertFalse(productsPage.cartIsEmptyTextIsDisplayed());
    }

    @Given("Be logged in and on the Home page")
    public void beLoggedInAndOnTheHomePage() {
        LoginSteps loginSteps = new LoginSteps();
        loginSteps.beOnLoginPage();
        loginPage = new LoginAndSignUpPage(driver);
        loginPage.enterEmailAddress("test.email@gmail.com");
        loginPage.enterPassword("Password123");
        loginPage.clickLogin();
    }

    @When("Add product in the cart and view cart")
    public void addProductInTheCartAndViewCart() {
        hoverOverFirstProduct();
        addToCartButtonIsDisplayed();
        clickOnAddToCart();
        confirmationProductHasBeenAddedToCartIsDisplayed();
        clickOnTheViewCartButton();
        userIsMovedToTheViewCartPage();
    }

    @Then("Proceed to checkout is displayed")
    public void proceedToCheckoutIsDisplayed() {
        System.out.println("Proceed to Checkout is displayed");
        Assert.assertTrue(productsPage.proceedToCheckoutButtonIsDisplayed());
    }

    @When("Click on Proceed to checkout button")
    public void clickOnProceedToCheckoutButton() {
        productsPage.clickOnProceedToCheckOutButton();
    }

    @Then("Checkout Information are displayed")
    public void checkoutInformationAreDisplayed() {
        Assert.assertTrue(productsPage.checkoutInformationSectionIsDisplayed());
    }

    @And("Place order button is displayed")
    public void placeOrderButtonIsDisplayed() {
        Assert.assertTrue(productsPage.placeOrderButtonIsDisplayed());
        System.out.println("Place order button is displayed");
    }

    @When("Click on place order button")
    public void clickOnPlaceOrderButton() {
        productsPage.clickOnPlaceOrderButton();
        System.out.println("Clicked on Place Order Button");
    }

    @Then("User is moved to Payment page")
    public void userIsMovedToPaymentPage() {
        System.out.println("User is moved to the view cart page");
        Assert.assertEquals("https://automationexercise.com/payment",driver.getCurrentUrl());
        System.out.println("User is on the Payment Page");
    }

    @When("User enter Payment details {string},{string},{string},{string},{string}")
    public void userEnterPaymentDetails(String cardName, String cardNumber, String cvc, String expirationMouth, String expirationYear) {
        paymentPage.enterNameOnTheCard(cardName);
        paymentPage.enterCardNumber(cardNumber);
        paymentPage.enterCVCInput(cvc);
        paymentPage.enterExpirationMouth(expirationMouth);
        paymentPage.enterExpirationYear(expirationYear);
        System.out.println("Payment details entered");
    }

    @And("Click on Pay and Confirm Order page")
    public void clickOnPayAndConfirmOrderPage() {
        paymentPage.clickOnPayAndConfirmOrderButton();
    }

    @Then("Congratulations! Your order has been confirmed! message is displayed")
    public void congratulationsYourOrderHasBeenConfirmedMessageIsDisplayed() {
        Assert.assertTrue(paymentPage.orderPlacedTextIsDisplayed());
    }
}
