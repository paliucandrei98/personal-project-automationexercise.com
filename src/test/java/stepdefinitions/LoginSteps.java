package stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.Cookie;
import selenium_pom.HomePage;
import selenium_pom.LoginAndSignUpPage;

import java.util.List;

import static stepdefinitions.Hooks.driver;


public class LoginSteps {
    LoginAndSignUpPage loginPage;
    HomePage homePage = new HomePage(driver);

    @Given("Be on the Login Page")
    public void beOnLoginPage(){
        System.out.println("Login page opened");
        driver.get(LoginAndSignUpPage.url);
        loginPage = new LoginAndSignUpPage(driver);
    }

    @When("Enter email address {string}")
    public void enterEmailAddress(String validEmailAddress) {
        System.out.println("Email address inserted");
        loginPage.enterEmailAddress(validEmailAddress);
    }

    @And("Enter password {string}")
    public void enterPassword(String password) {
        System.out.println("Password inserted");
        loginPage.enterPassword(password);
    }

    @And("Click on Login Button")
    public void clickOnLoginButton() {
        System.out.println("Click on Login button");
        loginPage.clickLogin();
    }

    @Then("Logout button is displayed")
    public void logoutButtonIsDisplayed() {
        System.out.println("Logout button is displayed");
        Assert.assertTrue("Logout button is not displayed", homePage.logoutButtonIsDisplayed());
    }

    @Then("'Email or password is incorrect' message is displayed")
    public void emailOrPasswordIsIncorrectMessageIsDisplayed() {
        Assert.assertTrue("'Email or password is incorrect' message is not displayed'" , loginPage.incorrectEmailAndPasswordMessageIsDisplayed());
    }

    @Then("Remain on the login page")
    public void remainOnTheLoginPage() {
        System.out.println("Remain on the LoginPage");
        Assert.assertEquals("https://automationexercise.com/login",driver.getCurrentUrl());
    }

    @Given("Be on the Home page")
    public void beOnTheHomePage() {
        System.out.println("Home page opened");
        driver.get(HomePage.url);
        homePage = new HomePage(driver);
    }

    @When("Add 'session id cookie' and refresh")
    public void addSessionIdCookieAndRefresh() {
        driver.get(HomePage.url);
        driver.manage().addCookie(new Cookie("sessionid","exsuka83ah71ggxlpwufgq18csvux0gn"));
        driver.navigate().refresh();

    }
    @When("User is logged with valid credentials")
    public  void addCredentialsAndPressLogIn(List<String> credentials){
        System.out.println("Credentials added from list");
        driver.get(LoginAndSignUpPage.url);
        loginPage = new LoginAndSignUpPage(driver);
        loginPage.enterEmailAddress(credentials.get(0));
        loginPage.enterPassword(credentials.get(1));
        loginPage.clickLogin();

    }

    @And("Delete 'session id cookie' cookie")
    public void deleteSessionIdCookieCookie() {
        driver.manage().deleteCookieNamed("sessionid");
        driver.navigate().refresh();
    }
}
