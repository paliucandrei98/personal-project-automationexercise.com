package stepdefinitions;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import selenium_pom.HomePage;

import java.time.Duration;

public class Hooks {
    public static WebDriver driver;

    @Before
    public void setup(){
        System.out.println("Setup method");
        driver = new ChromeDriver();
        int width = 1000;
        int height = 1200;
        Dimension dimension = new Dimension(width, height);
        driver.manage().window().setSize(dimension);
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(60));
    }

    @After
    public void cleanup(){
        System.out.println("Cleanup method");
        driver.close();
    }
    @After ("@Register-positive")
    public void deleteAccount(){
        HomePage homePage = new HomePage(driver);
       if (driver.findElement(homePage.deleteAccount).isDisplayed()){
           driver.findElement(homePage.deleteAccount).click();
       }
    }
}

