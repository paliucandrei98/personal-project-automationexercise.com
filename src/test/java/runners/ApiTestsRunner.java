package runners;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "C:\\Projects\\personal-project-automationexercise.com\\src\\test\\java\\features",
        glue = "stepdefinitions",
        tags = "@API",
        plugin = {"pretty"}
)
public class ApiTestsRunner {
    //*[@class="btn btn-default add-to-cart"]
}
