package runners;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "C:\\Projects\\personal-project-automationexercise.com\\src\\test\\java\\features",
        glue = "stepdefinitions",
        tags = "@Product",
        plugin = {"pretty","html:target/PositiveTests.html"}
)
public class ProductsTestsRunner {
}