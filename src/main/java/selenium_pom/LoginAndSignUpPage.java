package selenium_pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginAndSignUpPage {
    //driver
    private WebDriver driver;
    public static String url = "https://automationexercise.com/login";

    //selectors
    //Login
    public By loginEmailAddressInput = By.xpath("//*[@type=\"email\" and @data-qa=\"login-email\"]");
    public By passwordInput = By.xpath("//*[@type=\"password\"]");
    public By loginButton = By.xpath("//*[@data-qa=\"login-button\"]");
    public By incorrectEmailAndPasswordMessage = By.xpath("//*[@style=\"color: red;\"]");

    //Register
    public By registerName = By.xpath("//*[@type=\"text\"]");
    public By registerEmailAddress = By.xpath("//*[@type=\"email\"and@data-qa=\"signup-email\"]");
    public By newUserSignUpText = By.xpath("//*[@class=\"signup-form\"]//h2");
    public By signUpButton = By.xpath("//*[@data-qa=\"signup-button\"]");
    public By emailAddressAlreadyExistsText = By.xpath("//*[@style=\"color: red;\"]");

    //Constructors
    public LoginAndSignUpPage(WebDriver driver) {
        this.driver = driver;
    }

    //Actions
    public void enterEmailAddress(String username){
        driver.findElement(loginEmailAddressInput).sendKeys(username);
    }

    public void enterPassword(String password){
        driver.findElement(passwordInput).sendKeys(password);
    }

    public void clickLogin(){
        driver.findElement(loginButton).click();
    }

    public void enterName(String name){
        driver.findElement(registerName).sendKeys(name);
    }

    public void enterRegisterEmailAddress(String registerEmail){
        driver.findElement(registerEmailAddress).sendKeys(registerEmail);
    }

    public void clickSignUp() {
        driver.findElement(signUpButton).click();
    }

    public boolean incorrectEmailAndPasswordMessageIsDisplayed() {
        return driver.findElement(incorrectEmailAndPasswordMessage).isDisplayed();
    }

    public boolean newUserSignUpTextIsDisplayed(){
        return driver.findElement(newUserSignUpText).isDisplayed();
    }

    public boolean emailAddressAlreadyExistsText(){
        return driver.findElement(emailAddressAlreadyExistsText).isDisplayed();
    }
}
