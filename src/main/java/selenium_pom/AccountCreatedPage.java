package selenium_pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AccountCreatedPage {
    //driver
    private WebDriver driver;
    public static String url ="https://automationexercise.com/account_created";

    //selectors
    public By accountCreatedConfirmation = By.xpath("//*[@data-qa=\"account-created\"]");
    public By continueButton = By.xpath("//*[@class=\"btn btn-primary\"]");

    //constructor

    public AccountCreatedPage(WebDriver driver) {
        this.driver = driver;
    }
}
