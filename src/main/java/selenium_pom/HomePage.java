package selenium_pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage {
    //driver
    private WebDriver driver;
    public static String url = "https://automationexercise.com/";

    //selectors
    public By automationExerciseButton = By.xpath("//*[@alt=\"Website for automation practice\"]");
    public By homeButton = By.xpath("//*[@style=\"color: orange;\"]");
    public By productsButton = By.xpath("//*[@href=\"/products\"]");
    public By cartButton = By.xpath("//li/a[@href=\"/view_cart\"]");
    public By signUpAndLoginButton = By.xpath("//li/a[@href=\"/login\"]");
    public By testCasesButton = By.xpath("//li//a[@href=\"/test_cases\"]");
    public By apiTestingButton = By.xpath("//li//a[@href=\"/api_list\"]");
    public By videoTutorialsButton = By.xpath("//*[@href=\"https://www.youtube.com/c/AutomationExercise\"]");
    public By contactUsButton = By.xpath("//*[@href=\"/contact_us\"]");
    public By logoutButton = By.xpath("//*[@href=\"/logout\"]");
    public By deleteAccount = By.xpath("//*[@href=\"/delete_account\"]");

    //constructors
    public HomePage(WebDriver driver) {
        this.driver = driver;
    }

    //actions
    public boolean logoutButtonIsDisplayed() {
        return driver.findElement(logoutButton).isDisplayed();
    }

    public void clickLogoutButton() {
        driver.findElement(logoutButton).click();
    }
}


