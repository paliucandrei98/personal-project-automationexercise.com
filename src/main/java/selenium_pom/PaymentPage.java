package selenium_pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class PaymentPage {
    private WebDriver driver;
    public static String url = "https://automationexercise.com/payment";

    public By nameOnTheCardInput = By.xpath("//*[@name=\"name_on_card\"]");
    public By cardNumberInput = By.xpath("//*[@name=\"card_number\"]");
    public By cvcInput = By.xpath("//*[@name=\"cvc\"]");
    public By expirationMouth = By.xpath("//*[@name=\"expiry_month\"]");
    public By expirationYear = By.xpath("//*[@name=\"expiry_year\"]");
    public By payAndConfirmOrderButton = By.xpath("//*[@id=\"submit\"]");
    public By orderPlacedMessage = By.xpath("//*[@data-qa=\"order-placed\"]");

    public PaymentPage(WebDriver driver) {
        this.driver = driver;
    }

    //Actions
    public void enterNameOnTheCard(String name){
        driver.findElement(nameOnTheCardInput).sendKeys(name);
    }

    public void enterCardNumber(String number){
        driver.findElement(cardNumberInput).sendKeys(number);
    }

    public void enterCVCInput(String cvc){
        driver.findElement(cvcInput).sendKeys(cvc);
    }
    public void enterExpirationMouth(String mouth){
        driver.findElement(expirationMouth).sendKeys(mouth);
    }

    public void enterExpirationYear(String year){
        driver.findElement(expirationYear).sendKeys(year);
    }

    public void clickOnPayAndConfirmOrderButton(){
        driver.findElement(payAndConfirmOrderButton).click();
    }

    public boolean orderPlacedTextIsDisplayed(){
        return driver.findElement(orderPlacedMessage).isDisplayed();
    }


}
