package selenium_pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SignupPage {
    //driver
    private WebDriver driver;
    public static String url = "https://automationexercise.com/signup";

    //selectors
    public By name = By.cssSelector("#name");
    public By email = By.cssSelector("#email");
    public By passwordInput = By.cssSelector("#password");
    public By firstNameInput = By.cssSelector("#first_name");
    public By lastNameInput = By.cssSelector("#last_name");
    public By firstAddressInput = By.cssSelector("#address1");
    public By country = By.cssSelector("#country");
    public By stateInput = By.cssSelector("#state");
    public By cityInput = By.cssSelector("#city");
    public By zipcodeInput = By.cssSelector("#zipcode");
    public By mobileNumberInput = By.cssSelector("#mobile_number");
    public By createAccountButtonInput = By.xpath("//*[@data-qa=\"create-account\"]");
    public By accountCreatedText = By.xpath("//*[@class=\"title text-center\"]");
    public By continueButtonInput = By.xpath("//*[@class=\"btn btn-primary\"]");
    public By signupForm = By.xpath("//*[@class=\"login-form\"]");

    //constructors
    public SignupPage(WebDriver driver) {
        this.driver = driver;
    }

    //actions
    public void enterPassword(String password){
        driver.findElement(passwordInput).sendKeys(password);
    }
    public void enterFirstName(String firstName){
        driver.findElement(firstNameInput).sendKeys(firstName);
    }
    public void enterLastName(String lastName){
        driver.findElement(lastNameInput).sendKeys(lastName);
    }
    public void enterAddress(String address){
        driver.findElement(firstAddressInput).sendKeys(address);
    }
    public void enterState(String state){
        driver.findElement(stateInput).sendKeys(state);
    }
    public void enterCity(String city){
        driver.findElement(cityInput).sendKeys(city);
    }
    public void enterZipCode(String zipCode){
        driver.findElement(zipcodeInput).sendKeys(zipCode);
    }
    public void enterMobileNumber(String mobileNumber){
        driver.findElement(mobileNumberInput).sendKeys(mobileNumber);
    }
    public boolean signUpFormIsDisplayed(){
        return driver.findElement(signupForm).isDisplayed();
    }
    public void clickCreateAccount(){
        driver.findElement(createAccountButtonInput).click();
    }
    public void accountCreatedIsDisplayed(){
        driver.findElement(accountCreatedText).isDisplayed();
    }
    public void clickContinue(){
        driver.findElement(continueButtonInput).click();
    }
}
