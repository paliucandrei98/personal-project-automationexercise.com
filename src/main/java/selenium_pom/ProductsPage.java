package selenium_pom;

import dev.failsafe.internal.util.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ProductsPage {
    private WebDriver driver;
    public static String url = "https://automationexercise.com/products";

    //selectors
    public By searchProductInput = By.xpath("//*[@id=\"search_product\"]");
    public By searchProductButton = By.xpath("//*[@class=\"fa fa-search\"]");
    public By allProductsSection = By.xpath("//*[@class=\"title text-center\"]");
    public By searchedProductsSection = By.xpath("//*[@class=\"title text-center\"]");
    public By addToCartButton = By.xpath("//*[@class=\"btn btn-default add-to-cart\"]");
    public By confirmationAddProductToCart = By.xpath("//*[@id=\"cartModal\"]/div/div/div[2]/p[1]");
    public By continueShopping = By.xpath("//*[@class=\"btn btn-success close-modal btn-block\"]");
    public By viewCartFromConfirmationButton = By.xpath("//*[@href=\"/view_cart\"]/u");
    public By cartIsEmptyText = By.xpath("//*[@class=\"text-center\"]/b");
    public By proceedToCheckoutButton = By.xpath("//*[@class=\"btn btn-default check_out\"]");
    public By checkoutInformationSection = By.xpath("//*[@class=\"checkout-information\"]");
    public By placeOrderButton = By.xpath("//*[@href=\"/payment\"]");
    //constructors
    public ProductsPage(WebDriver driver) {
        this.driver = driver;
    }

    //Actions
    public void enterProductName(String product) {
        driver.findElement(searchProductInput).sendKeys(product);
    }

    public void clickOnSearchButton() {
        driver.findElement(searchProductButton).click();
    }

    public boolean setAllProductsSectionIsDisplayed() {
        return driver.findElement(allProductsSection).isDisplayed();
    }

    public boolean searchedProductsSectionIsDisplayed() {
        return driver.findElement(searchedProductsSection).isDisplayed();
    }
    public boolean addToCartButtonIsDisplayed(){
        return driver.findElement(addToCartButton).isDisplayed();
    }

    public void clickOnAddToCartButton(){
        driver.findElement(addToCartButton).click();
    }

    public boolean confirmationAddProductToCartIsDisplayed(){
        return  driver.findElement(confirmationAddProductToCart).isDisplayed();
    }

    public void clickOnContinueShoppingButton(){
        driver.findElement(continueShopping).click();
    }

    public void clickOnViewCartFromConfirmationButton(){
        driver.findElement(viewCartFromConfirmationButton).click();
    }

    public boolean cartIsEmptyTextIsDisplayed(){
        return driver.findElement(cartIsEmptyText).isDisplayed();
    }

    public boolean proceedToCheckoutButtonIsDisplayed(){
        return driver.findElement(proceedToCheckoutButton).isDisplayed();
    }

    public void clickOnProceedToCheckOutButton(){
        driver.findElement(proceedToCheckoutButton).click();
    }

    public boolean checkoutInformationSectionIsDisplayed(){
        return driver.findElement(checkoutInformationSection).isDisplayed();
    }

    public boolean placeOrderButtonIsDisplayed(){
        return driver.findElement(placeOrderButton).isDisplayed();
    }

    public void clickOnPlaceOrderButton(){
        driver.findElement(placeOrderButton).click();
    }

}

